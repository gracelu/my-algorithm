package com.grace.algor.greedy;

import org.junit.Assert;
import org.junit.Test;

public class AssignCookiesTest
{
   @Test
   public void testAssignCookies()
   {
      AssignCookies assignCookies = new AssignCookies();
      int[] greedy = {3, 1, 2, 4};
      int[] cookies = {4, 2, 2, 2};
      Assert.assertEquals(3, assignCookies.findContentChildren(greedy, cookies));
   }

   @Test
   public void testAssignCookiesEdge()
   {
      AssignCookies assignCookies = new AssignCookies();
      int[] greedy = new int[0];
      int[] cookies = {4, 2, 2, 2};
      Assert.assertEquals(0, assignCookies.findContentChildren(greedy, cookies));
   }
}
