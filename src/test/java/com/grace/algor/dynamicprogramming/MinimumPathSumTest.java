package com.grace.algor.dynamicprogramming;

import org.junit.Assert;
import org.junit.Test;

public class MinimumPathSumTest
{
   @Test
   public void testFindMinimumPathNull()
   {
      MinimumPathSum minimumPathSum = new MinimumPathSum();
      int[][] grid = null;
      Assert.assertEquals(0,minimumPathSum.minPathSum(grid));
   }

   @Test
   public void testFindMinimumPath()
   {
      MinimumPathSum minimumPathSum = new MinimumPathSum();
      int[][] grid = {{1, 2, 4}, {7, 9, 3}, {2, 1, 7}};
      Assert.assertEquals(17,minimumPathSum.minPathSum(grid));
   }
}
