package com.grace.algor.dynamicprogramming;

import org.junit.Assert;
import org.junit.Test;

public class MaximumSubarrayTest
{
   @Test
   public void testMaximumSubarray()
   {
      MaximumSubarray maximumSubarray = new MaximumSubarray();
      int[] array = {1, 4, 5, -5, 7, -4, 9, -11};
      int result = maximumSubarray.maxSubArray(array);
      Assert.assertEquals(17, result);
   }

   @Test
   public void testNullArray()
   {
      MaximumSubarray maximumSubarray = new MaximumSubarray();
      int[] array = null;
      int result = maximumSubarray.maxSubArray(array);
      Assert.assertEquals(0, result);
   }

   @Test
   public void testEmptyArray()
   {
      MaximumSubarray maximumSubarray = new MaximumSubarray();
      int[] array = {};
      int result = maximumSubarray.maxSubArray(array);
      Assert.assertEquals(0, result);
   }
}
