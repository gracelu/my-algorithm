package com.grace.algor.dynamicprogramming;

import org.junit.Assert;
import org.junit.Test;

public class ClimbingStairsTest
{
   @Test
   public void testclimbingStairsBadCase()
   {
      ClimbingStairs climbingStairs = new ClimbingStairs();
      long result = climbingStairs.climbStairsBad(5);
      Assert.assertEquals(8, result);
   }

   @Test
   public void testclimbingStairs()
   {
      ClimbingStairs climbingStairs = new ClimbingStairs();
      long result = climbingStairs.climbStairs(5);
      Assert.assertEquals(8, result);
   }
}
