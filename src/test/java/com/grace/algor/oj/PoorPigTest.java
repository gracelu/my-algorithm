package com.grace.algor.oj;

import org.junit.Assert;
import org.junit.Test;

public class PoorPigTest
{
   @Test
   public void testPoorPig()
   {
      PoorPig poorPig = new PoorPig();
      int result = poorPig.getPoorPigs(25, 15, 60);
      Assert.assertEquals(2, result);
   }
   @Test
   public void testPoorPigFail()
   {
      PoorPig poorPig = new PoorPig();
      int result = poorPig.getPoorPigs(25, 15, 14);
      Assert.assertEquals(-1, result);
   }
}
