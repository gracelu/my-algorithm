package com.grace.algor.tree;

import org.junit.Assert;
import org.junit.Test;

public class SymmetricTreeTest
{
   @Test
   public void doTestFalseSymmetric()
   {
      SymmetricTree symmetricTree = new SymmetricTree();

      TreeNode kid31 = new TreeNode(3, null, null);
      TreeNode kid32 = new TreeNode(4, null, null);
      TreeNode kid33 = new TreeNode(5, null, null);
      TreeNode kid34 = new TreeNode(3, null, null);
      TreeNode kid21 = new TreeNode(2, kid31, kid32);
      TreeNode kid22 = new TreeNode(2, kid33, kid34);
      TreeNode root = new TreeNode(1, kid21, kid22);
      Assert.assertFalse(symmetricTree.isSymmetric(root));
   }

   @Test
   public void doTestTrueSymmetric()
   {
      SymmetricTree symmetricTree = new SymmetricTree();

      TreeNode kid31 = new TreeNode(3, null, null);
      TreeNode kid32 = new TreeNode(4, null, null);
      TreeNode kid33 = new TreeNode(4, null, null);
      TreeNode kid34 = new TreeNode(3, null, null);
      TreeNode kid21 = new TreeNode(2, kid31, kid32);
      TreeNode kid22 = new TreeNode(2, kid33, kid34);
      TreeNode root = new TreeNode(1, kid21, kid22);
      Assert.assertTrue(symmetricTree.isSymmetric(root));
   }
}
