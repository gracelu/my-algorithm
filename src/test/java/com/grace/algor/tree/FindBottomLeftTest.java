package com.grace.algor.tree;

import org.junit.Assert;
import org.junit.Test;

public class FindBottomLeftTest
{
   @Test
   public void FindBottomTestOneNode()
   {
      TreeNode kid31 = new TreeNode(3, null, null);
      TreeNode kid32 = new TreeNode(4, null, null);
      TreeNode kid33 = new TreeNode(5, null, null);
      TreeNode kid34 = new TreeNode(3, null, null);
      TreeNode kid21 = new TreeNode(2, kid31, kid32);
      TreeNode kid22 = new TreeNode(2, kid33, kid34);
      TreeNode root = new TreeNode(1, kid21, kid22);
      FindBottomLeft findBottomLeft = new FindBottomLeft();
      Assert.assertEquals(3, findBottomLeft.FindBottomLeft(root));
   }

   @Test
   public void FindBottomTest()
   {
      TreeNode root = new TreeNode(1, null, null);
      FindBottomLeft findBottomLeft = new FindBottomLeft();
      Assert.assertEquals(1, findBottomLeft.FindBottomLeft(root));
   }
}
