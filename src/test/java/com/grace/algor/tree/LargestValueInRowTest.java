package com.grace.algor.tree;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class LargestValueInRowTest
{
   @Test
   public void FindBottomTestOneNode()
   {
      TreeNode kid31 = new TreeNode(3, null, null);
      TreeNode kid32 = new TreeNode(4, null, null);
      TreeNode kid33 = new TreeNode(5, null, null);
      TreeNode kid34 = new TreeNode(3, null, null);
      TreeNode kid21 = new TreeNode(2, kid31, kid32);
      TreeNode kid22 = new TreeNode(9, kid33, kid34);
      TreeNode root = new TreeNode(1, kid21, kid22);
      LargestValueInRow largestValueInRow = new LargestValueInRow();
      Assert.assertEquals(Arrays.asList(1,9,5), largestValueInRow.LargestValues(root));
   }

   @Test
   public void FindBottomTest()
   {
      TreeNode root = null;
      LargestValueInRow largestValueInRow = new LargestValueInRow();
      Assert.assertEquals(0, largestValueInRow.LargestValues(root).size());
   }
}
