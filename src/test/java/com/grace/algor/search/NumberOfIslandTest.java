package com.grace.algor.search;

import org.junit.Assert;
import org.junit.Test;

public class NumberOfIslandTest
{
   @Test
   public void testIslandNumberOneIsland()
   {
      char[][] map = {{'1'}};
      NumberOfIsland numberOfIsland = new NumberOfIsland();
      int result = numberOfIsland.numIslands(map);
      Assert.assertEquals(1, result);
   }

   @Test
   public void testIslandNumberNoIsland()
   {
      char[][] map = {{'0'}};
      NumberOfIsland numberOfIsland = new NumberOfIsland();
      int result = numberOfIsland.numIslands(map);
      Assert.assertEquals(0, result);
   }

   @Test
   public void testIslandNumberNullIsland()
   {
      char[][] map = null;
      NumberOfIsland numberOfIsland = new NumberOfIsland();
      int result = numberOfIsland.numIslands(map);
      Assert.assertEquals(0, result);
   }

   @Test
   public void testIslandNumberNormal()
   {
      char[][] map = {{'1', '1', '0'}, {'0', '0', '1'}, {'0', '1', '0'}};
      NumberOfIsland numberOfIsland = new NumberOfIsland();
      int result = numberOfIsland.numIslands(map);
      Assert.assertEquals(3, result);
   }
}

