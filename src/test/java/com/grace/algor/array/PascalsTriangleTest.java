package com.grace.algor.array;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PascalsTriangleTest
{
   @Test
   public void testPascalsTriangle()
   {
      PascalsTriangle pascalsTriangle = new PascalsTriangle();
      List<List<Integer>> result = pascalsTriangle.generate(5);
      List<List<Integer>> expect = new ArrayList<List<Integer>>();
      expect.add(Arrays.asList(1));
      expect.add(Arrays.asList(1, 1));
      expect.add(Arrays.asList(1, 2, 1));
      expect.add(Arrays.asList(1, 3, 3, 1));
      expect.add(Arrays.asList(1, 4, 6, 4, 1));
      boolean testResult = true;
      if (result.size() == expect.size())
      {
         for (int i = 0; i < result.size(); i++)
            testResult &= result.get(i).equals(expect.get(i));
         Assert.assertTrue(testResult);
      }else
      {
         Assert.fail();
      }

   }

   @Test
   public void testPascalsTriangleEdge()
   {
      PascalsTriangle pascalsTriangle = new PascalsTriangle();
      List<List<Integer>> result = pascalsTriangle.generate(0);
      List<List<Integer>> expect = new ArrayList<List<Integer>>();
      boolean testResult = true;
      if (result.size() == expect.size())
      {
         for (int i = 0; i < result.size(); i++)
            testResult &= result.get(i).equals(expect.get(i));
         Assert.assertTrue(testResult);
      }else
      {
         Assert.fail();
      }
   }
}
