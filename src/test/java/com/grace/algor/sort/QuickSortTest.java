package com.grace.algor.sort;

import org.junit.*;

public class QuickSortTest
{
   @Test
   public void doTestQuickSort()
   {
      int[] input = {24, 2, 45, 20, 56, 75, 2, 56, 99, 53, 12};
      int[] result = {2, 2, 12, 20, 24, 45, 53, 56, 56, 75, 99};
      QuickSort quickSort = new QuickSort();
      quickSort.doQuickSort(input, 0, input.length);
      Assert.assertArrayEquals(result, input);
   }
}
