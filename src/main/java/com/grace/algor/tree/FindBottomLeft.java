package com.grace.algor.tree;

import java.util.LinkedList;
import java.util.Queue;

/**
 * This question from leetcode 513.
 */
public class FindBottomLeft
{
   /**
    * We assume that root node is not null. The time complexity is O(N). The space complexity is O(N).
    * @param root input is the root node of a tree
    * @return return the value of left bottom
    */
   public int FindBottomLeft(TreeNode root)
   {
      int result = root.val;
      Queue<TreeNode> nodes = new LinkedList();
      nodes.offer(root);
      while(nodes.size()!=0)
      {
         int levelCount = nodes.size();
         for (int i = 0; i < levelCount; i++)
         {
            TreeNode current = nodes.poll();
            if (i == 0)
               result = current.val;
            if (current.left != null)
               nodes.offer(current.left);
            if (current.right != null)
               nodes.offer(current.left);
         }
      }
      return result;
   }
}
