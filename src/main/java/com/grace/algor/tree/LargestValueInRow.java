package com.grace.algor.tree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * This question from leetcode 515.
 */
public class LargestValueInRow
{
   /**
    * The input might be null.
    * @param root the root node of the tree
    * @return return a list has the largest values in each row.
    */
   public List<Integer> LargestValues(TreeNode root)
   {
      List<Integer> result = new ArrayList<Integer>();
      if (root == null)
         return result;
      Queue<TreeNode> nodes = new LinkedList<TreeNode>();
      nodes.offer(root);
      while(nodes.size() > 0)
      {
         int currentNodes = nodes.size();
         int max = nodes.peek().val;
         for (int i = 0; i < currentNodes; i++)
         {
            TreeNode node = nodes.poll();
            if (max < node.val)
               max = node.val;
            if (node.right != null)
               nodes.offer(node.right);
            if (node.left!=null)
               nodes.offer(node.left);
         }
         result.add(max);
      }
      return result;
   }
}
