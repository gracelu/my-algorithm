package com.grace.algor.sort;

/**
 * This is a implementation of in-place quick sort
 * 1. Choose last number as pivot.
 * 2. Put number into two group by comparing to the pivot.
 * 3. Swap the next position of the end the little number with pivot.
 * 4. Then the pivot is in the right position
 *
 * Time complexity of Quick sorting is NLogN, average and the best are NLogN. The worst situation is N^2.
 * Space complexity of every round is O(1), so the total space complexity is O(logN).
 *
 * When using Arrays.sort to sort a primitive type, it use quick sort.
 */
public class QuickSort
{
   public void doQuickSort(int[] source, int left, int right)
   {
      if (left == right)
         return;

      int pivot = right - 1;
      int position = left;
      for (int i = left; i < right - 1; i++)
      {
         if(source[pivot] > source[i])
         {
            swap(source, i, position);
            position++;
         }
      }
      swap(source, position, pivot);
      if(position == 10)
      {
         System.out.println(10);
      }
      doQuickSort(source,position + 1, right);
      doQuickSort(source,left, position);
   }

   private void swap(int[] source, int index1, int index2)
   {

      int temp = source[index1];
      source[index1] = source[index2];
      source[index2] = temp;
   }
}
