package com.grace.algor.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PascalsTriangle
{
   /**
    * This question comes from leet code 118.
    * Given numRows, generate the first numRows of Pascal's triangle.
    * For example, given numRows = 5, return:
    * [[1],[1,1],[1,2,1],[1,3,3,1],[1,4,6,4,1]]
    * @param numRows input the number of rows you want.
    * @return return a pascal's triangle.
    */
   public List<List<Integer>> generate(int numRows)
   {
      List<List<Integer>> result = new ArrayList<List<Integer>>();
      if (numRows == 0)
         return result;
      List<Integer> cache = new ArrayList<Integer>();
      for(int i = 0; i < numRows; i++)
      {
         cache.add(1);
         for(int j = cache.size() - 2; j > 0; j--)
         {
            cache.set(j, cache.get(j) + cache.get(j - 1));
         }
         result.add(new ArrayList<Integer>(cache));
      }
      return result;
   }
}
