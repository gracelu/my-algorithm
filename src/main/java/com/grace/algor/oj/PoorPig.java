package com.grace.algor.oj;

public class PoorPig
{
   /**
    * This question comes from leetcode 458.
    * If there are n buckets and a pig drinking poison will die within m minutes,
    * how many pigs (x) you need to figure out the "poison" bucket within p minutes?
    * There is exact one bucket with poison.
    * @param buckets The buckets number.
    * @param minutesToDie how many minutes to die after the pigs drink the water?
    * @param minutesToTest how many minutes you have to find out the poison bucket.
    * @return the minimum number of pigs you need to test.
    */
   public int getPoorPigs(int buckets, int minutesToDie, int minutesToTest)
   {
      if (minutesToDie > minutesToTest || minutesToDie == 0 || buckets == 0)
         return -1;
      int base = minutesToTest / minutesToDie + 1;
      int pig = 1;
      while (Math.pow(base, pig) < buckets)
      {
         pig++;
      }
      return pig;
   }
}
