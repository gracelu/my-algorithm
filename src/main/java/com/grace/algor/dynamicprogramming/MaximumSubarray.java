package com.grace.algor.dynamicprogramming;


public class MaximumSubarray
{
   public int maxSubArray(int[] nums)
   {
      if (nums == null || nums.length == 0)
      {
         return 0;
      }
      int max = nums[0];
      int current = nums[0];
      for (int i = 0; i < nums.length - 1; i++)
      {
         current = Math.max(current + nums[i + 1], nums[i + 1]);
         max = Math.max(max, current);
      }
      return max;
   }
}
