package com.grace.algor.dynamicprogramming;

import java.util.Arrays;

public class ClimbingStairs
{
   /**
    * A basic recursive could solve this problem, but you'll wasting a lot of computing time to calculate the same number.
    */
   public int climbStairsBad(int n)
   {
      if (n < 3)
         return n;
      return climbStairs(n - 1) + climbStairs(n - 2);
   }


   /**
    * This method will store the number in a queue, then it don't have to do unnecessary calculation.
    * @param n
    * @return
    */
   public int climbStairs(int n) {
      if (n < 3)
         return n;
      int[] map = new int[n];
      Arrays.fill(map, -1);
      map[0] = 1;
      map[1] = 2;
      return StairsHelper(map, n - 1);
   }

   public int StairsHelper(int[] map, int number)
   {
      if(map[number] == -1)
         map[number] = StairsHelper(map, number - 1) + StairsHelper(map, number -2);
      return map[number];
   }
}
