package com.grace.algor.dynamicprogramming;

/**
 *This question from leetcode 64.
 */
public class MinimumPathSum
{
   /**
    * Given a m x n grid filled with non-negative numbers,
    * find a path from top left to bottom right which minimizes the sum of all numbers along its path.
    * Note: You can only move either down or right at any point in time.
    * The space complexity is O(1).
    * The time complexity is O(m*n).
    * @param grid the input grid.
    * @return minimum path number.
    */
   public int minPathSum(int[][] grid)
   {
      if (grid == null)
         return 0;
      for (int i = 0;i < grid.length; i++)
      {
         for (int j = 0; j < grid[i].length; j++)
         {
            if (i == 0 && j==0)
               continue;
            if (j == 0)
               grid[i][j] = grid[i][j] + grid[i - 1][j];
            if (i == 0)
               grid[i][j] = grid[i][j] + grid[i][j - 1];
            if (j > 0 && i > 0)
               grid[i][j] = grid[i][j] + Math.min(grid[i][j - 1],grid[i - 1][j]);
         }
      }
      return grid[grid.length - 1][grid[0].length - 1];
   }
}
