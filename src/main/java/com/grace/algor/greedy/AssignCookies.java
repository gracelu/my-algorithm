package com.grace.algor.greedy;

import java.util.Arrays;

public class AssignCookies
{
   /**
    * This question comes from leetcode 455.
    * Assume you are an awesome parent and want to give your children some cookies.
    * But, you should give each child at most one cookie.
    * Each child i has a greed factor gi, which is the minimum size of a cookie that the child will be content with;
    * and each cookie j has a size sj.
    * If sj >= gi, we can assign the cookie j to the child i, and the child i will be content.
    * Your goal is to maximize the number of your content children and output the maximum number.
    *
    * Note:
    * You may assume the greed factor is always positive.
    * You cannot assign more than one cookie to one child.
    * Example:
    * Input: [1,2], [1,2,3]
    * Output: 2
    * @param g the kids number with greedy factor.
    * @param s the cookies with different sizes.
    * @return the number of the kids that could be satisfied.
    */
   public int findContentChildren(int[] g, int[] s)
   {
      if (g.length == 0 || s.length == 0)
         return 0;
      Arrays.sort(g);
      Arrays.sort(s);
      int kidIndex = 0;
      for (int i = 0;i < s.length; i++)
      {
         if (kidIndex == g.length)
            break;
         else if (g[kidIndex] <= s[i])
            kidIndex++;
      }
      return kidIndex;
   }
}
