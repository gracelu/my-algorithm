package com.grace.algor.search;

/**
 * This question from leetcode 200.
 */
public class NumberOfIsland
{
   /**
    * We have a map as follow:
    * 1000
    * 1011
    * 0100
    * The number of island is 3.
    * Please find out the number of given map.
    * @param grid
    * @return
    */
   public int numIslands(char[][] grid)
   {
      int count = 0;
      if(grid == null)
         return count;
      for (int i = 0; i < grid.length; i++)
      {
         for (int j = 0; j < grid[i].length; j++)
         {
            if (checkIsIsland(grid, i, j))
               count++;
         }
      }
      return count;
   }

   private boolean checkIsIsland(char[][] grid, int x, int y)
   {
      boolean result = false;
      if (x >= 0 && y >= 0 && x < grid.length && y < grid[0].length && grid[x][y] == '1')
      {
         grid[x][y] = 'm';
         checkIsIsland(grid, x, y + 1);
         checkIsIsland(grid, x, y - 1);
         checkIsIsland(grid, x + 1, y);
         checkIsIsland(grid, x - 1, y);
         result = true;
      }
      return result;
   }
}
